<?php
if (!isset($_SESSION)) {
    session_start();
}
include_once("connection.php");

if (isset($_SESSION["user"])) { //Redirect already logged users to frontpage
    header("Location: /index.php");
}

$loginerror = "";
if ((isset($_POST["extUser"])) && (isset($_POST["extPass"]))) {
    $stmt = $pdo->prepare('SELECT uid, pwhash FROM users WHERE username = ?');
    $stmt->execute([$_POST["extUser"]]);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    $hash = $data['pwhash'];
    $uid  = $data['uid'];

    if (!(empty($hash))) { //If user exists
        if (password_verify($_POST["extPass"], $hash)) { //Check hash
            $_SESSION["user"] = $_POST["extUser"]; //If OK, set session variables and log user in
            $_SESSION["uid"]  = $uid;
            header("Location: /index.php");
        } else {
            $loginerror = "</br>Väärä käyttäjätunnus tai salasana";
        }
    } else {
        $loginerror = "</br>Väärä käyttäjätunnus tai salasana";
    }
} else {
    $loginerror = "</br>Syötä käyttäjätunnus ja salasana";
}

?>
<!DOCTYPE html>
<head>
    <title>Kirjaudu sisään</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="istyle.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<nav class="topnav" id="navBar" role="full-horizontal">
    <a href="/index.php">Skinnarilan opiskelijatapahtumat</a>
    <a href="/calendar.php" title="Kalenteri">Kalenteri</a>
    <a href="/login.php" title="Kirjaudu">Kirjaudu</a>
    <a href="/newuser.php" title="Luo käyttäjä">Luo käyttäjä</a>
    <a class="navBarIcon" href="javascript:void(0);" style="font-size: 20px" onclick="openMenu()">&#8744;</a>
</nav>

<body>
    <div id="newUserDiv">
        <p>Kirjaudu sisään olemassa olevalle käyttäjälle</p>
        <form id="createUserForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <input id="extUser" type="text" placeholder="Käyttäjätunnus" name="extUser">
            <input id="extPass" type="password" placeholder="Salasana" name="extPass">
            <button id="createUserButton" type="submit">Kirjaudu</button>
        </form>
        <div id="phperror">
            <?php echo $loginerror; ?>
        </div>
    </div>
    <script>function openMenu() {
         let el = document.getElementById("navBar");
         if (el.className === "topnav") {
             el.className += " responsive";
         } else {
             el.className = "topnav";
         }
    }</script>
    <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
</body>
