<?php
if (!isset($_SESSION)) {
    session_start();
}
include('connection.php');
try {
    $stmt = $pdo->prepare('INSERT INTO events (edesc, ewhere, ewhen, ename, eprice) VALUES (:eDesc, :eWhere, :eWhen, :eName, :ePrice)');
    $stmt->execute(array(':eDesc'  => $_POST["eDesc"],
                         ':eWhere' => $_POST["eWhere"],
                         ':eWhen'  => $_POST["eWhen"],
                         ':eName'  => $_POST["eName"],
                         ':ePrice' => $_POST["ePrice"]));
} catch (PDOException $e) {
    echo "Error when inserting event to database</br>";
}
?>