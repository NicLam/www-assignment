<!-- Frontpage of the application -->
<!-- Created by Niclas Lamponen 0455946 -->
<!-- All the tables are generated by PHP with data from the SQL database.  -->
<?php
if (!isset($_SESSION)) {
    session_start();
}?>
<!DOCTYPE html>

<head>
    <title>Skinnarilan opiskelijatapahtumat</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="istyle.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<nav class="topnav" id="navBar" role="full-horizontal">
    <a href="#">Skinnarilan opiskelijatapahtumat</a>
    <a href="/calendar.php" title="Kalenteri">Kalenteri</a>
    <?php if (isset($_SESSION["user"])) {
        echo '<a href="/newevent.php" title="Luo tapahtuma">Luo tapahtuma</a>';
        echo '<a href="/logout.php" title="Kirjaudu ulos">Kirjaudu ulos</a>';
    } else {
        echo '<a href="/login.php" title="Kirjaudu">Kirjaudu</a>';
        echo '<a href="/newuser.php" title="Luo käyttäjä">Luo käyttäjä</a>';
    }?>
    <a class="navBarIcon" href="javascript:void(0);" style="font-size: 20px" onclick="openMenu()">&#8744;</a>
</nav>

<!-- bdoy with one container with 3 containers side by side displaying data. Responsiveness is handled by css -->
<body style="font-family: Roboto, sans-serif; font-size: 16px; font-weight: 400; height: 100%; padding: 0px auto; min-width: 360px; background: #f9f9f9;">
    <div class="container" style="font-size: 0; margin-left: auto; margin-right: auto;">
        <div class="column" style="min-width: 360px; width: 30%; font-size: 16px; display: inline-block; vertical-align: top; box-sizing: border-box; text-align: center;" id="weather"></br>
            <?php include 'getweather.php'; ?>
        </div>
        <div class="column" style="min-width: 360px; width: 40%; font-size: 16px; display: inline-block; vertical-align: top; box-sizing: border-box; text-align: center;" id="events">
        </br>
            <?php include 'displayevents.php'; ?>
        </div>
        <div class="column" style="min-width: 360px; width: 30%; font-size: 16px; display: inline-block; vertical-align: top; box-sizing: border-box; text-align: center;" id="attending">
           </br><table class="datatable weatherinfo">
                <thead>
                    <tr>
                        <th class="weatherth" colspan="2">Omat osallistumiset</th>
                    </tr>
                </thead>
                <tbody class="table-hover" id="attendingTable">
                    <?php if (isset($_SESSION["user"])) {
                            include 'displayattending.php';
                        } else {
                            echo '<td colspan="2">Kirjaudu sisään tallentaaksesi osallistumiset</td>';
                        }?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- This script is for opening the menu when it's minimized, just adds one class -->
    <script>function openMenu() {
         let el = document.getElementById("navBar");
         if (el.className === "topnav") {
             el.className += " responsive";
         } else {
             el.className = "topnav";
         }
     }</script>
    <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
</body>
