<?php
//Creates table that will be display the events current user is attending
if (!isset($_SESSION)) {
    session_start();
}
include("connection.php");
if (isset($_SESSION["uid"])) { //Check login
    try {
        $stmt = $pdo->prepare('SELECT events.ename, events.ewhen FROM users INNER JOIN attending on users.uid = attending.uid INNER JOIN events on attending.eid = events.eventid WHERE users.uid = ?');
        $stmt->execute([$_SESSION["uid"]]);
        //Create tablerow elements for each event
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr>
            <td class="weathertd">' . htmlspecialchars($row["ename"], ENT_QUOTES, 'UTF-8') . '</td>
            <td class="weathertd"> ' .htmlspecialchars(substr($row["ewhen"], 0, -3), ENT_QUOTES, 'UTF-8') . '</td>
            </tr>';
        }
    } catch (PDOException $e) {
        echo 'Tietoja osallistumisesta ei pystytty hakemaan';
    }
}
