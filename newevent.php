<?php
if (!isset($_SESSION)) {
    session_start();
}
include_once("connection.php");

if (!isset($_SESSION["user"])) { //Redirect unlogged users to frontpage
    header("Location: /index.php");
}?>
<!DOCTYPE html>
<head>
    <title>Luo uusi tapahtuma</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="istyle.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<nav class="topnav" id="navBar" role="full-horizontal">
    <a href="/index.php">Skinnarilan opiskelijatapahtumat</a>
    <a href="/calendar.php" title="Kalenteri">Kalenteri</a>
    <?php if (isset($_SESSION["user"])) { //Logged in users see different header
        echo '<a href="/newevent.php" title="Luo tapahtuma">Luo tapahtuma</a>';
        echo '<a href="/logout.php" title="Kirjaudu ulos">Kirjaudu ulos</a>';
    } else {
        echo '<a href="/login.php" title="Kirjaudu">Kirjaudu</a>';
        echo '<a href="/newuser.php" title="Luo käyttäjä">Luo käyttäjä</a>';
    }?>
    <a class="navBarIcon" href="javascript:void(0);" style="font-size: 20px" onclick="openMenu()">&#8744;</a>
</nav>

<body>
    <div id="newUserDiv">
        <p>Luo uusi tapahtuma</p>
        <form id="createEventForm" method="POST">
            <input class="newEvent" id="eName" name="eName" maxlength="256" placeholder="Tapahtuman nimi (pakollinen)">
            <input class="newEvent" id="eWhere" name="eWhere" maxlength="256" placeholder="Tapahtuman sijainti (pakollinen)">
            <input id="eWhen" style="width: 400px" class="flatpickr flatpickr-input-active" type="text" placeholder="Valitse pvm">
            <input class="newEvent" id="ePrice" name="ePrice" maxlength="256" placeholder="Tapahtuman hinta">
            <textarea class="newEvent" id="eDesc" name="eDesc" maxlength="2048" placeholder="Tapahtuman kuvaus (pakollinen)" maxlength="2048" rows="8"></textarea>
            <button id="createUserButton" type="submit">Luo</button>
        </form>
    </div>
    <script>function openMenu() {
         let el = document.getElementById("navBar");
         if (el.className === "topnav") {
             el.className += " responsive";
         } else {
             el.className = "topnav";
         }
     }</script>
    <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="newevent.js"></script>
    <script type="text/javascript" src="script.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
</body>
