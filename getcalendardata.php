<?php
/* This module is used to get all the events from the database
 * It will add them to JSON encoded array which can be parsed by FullCalendar
 */
include('connection.php');
try {
    $jsonEvents = array(); //Create array that will be returned
    $stmt = $pdo->prepare('SELECT eventid, ename, ewhen FROM events');
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $a = array();
        $a['id']    = htmlspecialchars($row['eventid'], ENT_QUOTES, 'UTF-8');
        $a['title'] = htmlspecialchars($row['ename'], ENT_QUOTES, 'UTF-8');
        $a['date']  = htmlspecialchars($row['ewhen'], ENT_QUOTES, 'UTF-8');
        array_push($jsonEvents, $a); //Add each element into the array
    }
    echo json_encode($jsonEvents);
    exit();
} catch (PDOException $e) {
    echo 'Virhe kalenteria täyttäessä';
}