$(function() {
    'use strict';
    $('#createEventForm').on('click', '#createUserButton', function() {
        let eWhere = $('#eWhere').val();
        let eWhen  = $('#eWhen').val();
        let eName  = $('#eName').val();
        let ePrice = $('#ePrice').val();
        let eDesc  = $('#eDesc').val();

        if (eWhere && eWhen && eDesc && eName) {
            let request = $.ajax({ //AJAX request to insert this into database
                url: "insertNewEvent.php",
                type: "POST",
                data: {
                    'eWhere': eWhere,
                    'eWhen' : eWhen,
                    'eName' : eName,
                    'ePrice': ePrice,
                    'eDesc' : eDesc
                },
                dataType: "html",
                success: function() {
                    let el = document.createElement("li"); //XSS prevention
                    el.setAttribute("style", "padding:0; list-style-type:none;");
                    el.innerText = "Tapahtuma " + eName + " luotu onnistuneesti.";
                    $('#createEventForm').append(el); //Tell user about successful event creation
                    $('.newEvent').val("");
                },
                error: function() {
                    $('#createEventForm').after('<p>Virhe tapahtumaa luodessa. Ota yhteys ylläpitääjän. (tai älä, ei tätä sivustoa kukaan ylläpidä)</p>');
                }
            });
        } else {
            $('#createEventForm').after('<p>Syötä pakolliset kentät</p>');
        }
    });

    $('#createEventForm').on('submit', function(e) { //Don't refresh page on form submit
        e.preventDefault();
    });

    //Initialize datetimepicker
    $(".flatpickr").flatpickr({
        enableTime: true,
        dateFormat: 'Y-m-d H:i',
        time_24hr: true
    });
});
