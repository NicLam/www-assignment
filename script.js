$(function() {
    //Method to handle users' clicks when they click on "attending" -button
    "use strict";
    $('.datatable').on('click', '.attend', function(e) {
        let eventid = this.id;
        let request = $.ajax({
            url: "insertAttending.php",
            type: "POST",
            data: {
                'eid': eventid
            },
            success: function() {
                    $("#attendingTable").load("displayattending.php"); //Refresh the table without page reload
            },
            error: function() {
                $("#attending").after("<p>Virhe syöttäessä osallistumista tietokantaan</p>");
            }
        });
    });
});
