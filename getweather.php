<?php
//Module for getting weather from memcached or alternatevily from openweathermap API
$mc = new Memcached();
$mc->addServer("127.0.0.1", 11211) or die("Error in memcached connection");
$result = $mc->get('Lappeenranta'); //Get result from Mc
if (!$result) { //If entry doesn't exist in Mc, create a new one
    $url = "http://api.openweathermap.org/data/2.5/forecast?q=Lappeenranta&appid=2ec3ef7a7e946e7776beed6f9053ad17&units=metric";
    $json = file_get_contents($url);
    $data = json_decode($json, true); //Get URL contents JSON encoded

    if ($data != NULL) { //Parse JSON and place into Mc
        $mc->set('Lappeenranta', array( //Admittedly it looks like shit.
            'day0weather' => $data['list'][0]['weather'][0]['description'],
            'day0temp'    => $data['list'][0]['main']['temp'],
            'day0icon'    => $data['list'][0]['weather'][0]['icon'],
            'day0date'    => $data['list'][0]['dt_txt'],
            'day1weather' => $data['list'][8]['weather'][0]['description'],
            'day1temp'    => $data['list'][8]['main']['temp'],
            'day1icon'    => $data['list'][8]['weather'][0]['icon'],
            'day1date'    => $data['list'][8]['dt_txt'],
            'day2weather' => $data['list'][16]['weather'][0]['description'],
            'day2temp'    => $data['list'][16]['main']['temp'],
            'day2icon'    => $data['list'][16]['weather'][0]['icon'],
            'day2date'    => $data['list'][16]['dt_txt'],
            'day3weather' => $data['list'][24]['weather'][0]['description'],
            'day3temp'    => $data['list'][24]['main']['temp'],
            'day3icon'    => $data['list'][24]['weather'][0]['icon'],
            'day3date'    => $data['list'][24]['dt_txt'],
            'day4weather' => $data['list'][32]['weather'][0]['description'],
            'day4temp'    => $data['list'][32]['main']['temp'],
            'day4icon'    => $data['list'][32]['weather'][0]['icon'],
            'day4date'    => $data['list'][32]['dt_txt']
        ), 1800); //Update every 0.5h
        $result = $mc->get('Lappeenranta');
    } else {
        echo '<p>Säätietojen hakeminen epäonnistui</p>';
    }
}

for ($i = 0; $i <= 4; $i++) { //Post upcoming weather on the front page
    $weather = $result['day' . $i . 'weather'];
    $temp    = $result['day' . $i . 'temp'];
    $icon    = $result['day' . $i . 'icon'];
    $date    = $result['day' . $i . 'date'];
    echo '
        <table class="datatable weatherinfo">
        <thead>
        <tr>
        <th class="weatherth" colspan="2">' . substr($date, 0, -3) . '</th>
        </tr>
        </thead>
        <tbody class="table-hover">
        <tr>
        <td class="weathertd">Sää</td>
        <td class="weathertd"> ' . $weather . '</td>
        </tr>
        <tr>
        <td class="weathertd">Lämpötila</td>
        <td class="weathertd">' . $temp . '</td>
        </tr>
        <tr>
        </tbody>
        </table>
        ';
}

if ($result['day0temp'] <= 0) { //Friendly reminder
    echo '<p>Älkää sammuko hankeen, ulkona on viileää</p>';
}