<!-- Page for displaying the calendar -->
<?php
if (!isset($_SESSION)) {
    session_start();
}?>
<!DOCTYPE html>
<head>
    <title>Kalenteri</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="istyle.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel='stylesheet' href='fullcalendar/fullcalendar.min.css' />
    <script src='jquery-3.2.1.min.js'></script> <!-- scripts loaded here because FullCalendar wants them in this order -->
    <script src='fullcalendar/moment.js'></script>
    <script src='fullcalendar/fullcalendar.js'></script>
</head>

<nav class="topnav" id="navBar" role="full-horizontal">
    <a href="/index.php">Skinnarilan opiskelijatapahtumat</a>
    <a href="/calendar.php" title="Kalenteri">Kalenteri</a>
    <?php if (isset($_SESSION["user"])) {
        echo '<a href="/newevent.php" title="Luo tapahtuma">Luo tapahtuma</a>';
        echo '<a href="/logout.php" title="Kirjaudu ulos">Kirjaudu ulos</a>';
    } else {
        echo '<a href="/login.php" title="Kirjaudu">Kirjaudu</a>';
        echo '<a href="/newuser.php" title="Luo käyttäjä">Luo käyttäjä</a>';
    }?>
    <a class="navBarIcon" href="javascript:void(0);" style="font-size: 20px" onclick="openMenu()">&#8744;</a>
</nav>

<body>
    <div id="calendar" style="width: 60%; margin:auto;"></div>
    <!-- //Function for initializing calendar. Calendar data in events will be JSON from by getcalendardata.php -->
    <script>$('#calendar').fullCalendar({
    weekends: true,
    height: 800,
    events: {
        url: "getcalendardata.php",
        type: "POST",
        error: function() {
            alert("Tietojen hakeminen kalenteriin epäonnistui");
        }
    }
     });</script>
    <script>function openMenu() {
         let el = document.getElementById("navBar");
         if (el.className === "topnav") {
             el.className += " responsive";
         } else {
             el.className = "topnav";
         }
     }</script>
</body>
