<?php
if (!isset($_SESSION)) {
    session_start();
}
include_once("connection.php");

if (isset($_SESSION["user"])){
    header("Location: /index.php");
}

$error = "";
$continue = 1;

//If form was submitted and fields set
if ((isset($_POST["confirmPass"])) && (isset($_POST["newUser"])) && (isset($_POST["newPass"]))) {
    //Check for existing users with SQL statement
    $pwtext = $_POST["newPass"];
    $stmt = $pdo->prepare('SELECT 1 FROM users WHERE username = ?');
    $stmt->execute([$_POST["newUser"]]);
    $userExists = $stmt->fetchColumn();

    if ($_POST["newPass"] != $_POST["confirmPass"]) { //Make sure passwords match
        $error .= "Salasanat eivät täsmää</br>";
        $continue = 0;
    }

    if (!ctype_alnum($_POST["newUser"]) || (strlen($_POST["newUser"]) > 32)) { //Alphanumeric username and length check
        $error .= "Käyttäjänimessä on virhe (kiellettyjä merkkejä tai pituus)</br>";
        $continue = 0;
    }

    if ($userExists) { //Check for existing user
        $error .= "Käyttäjänimi on varattu</br>";
        $continue = 0;
    }

    if (((strlen($pwtext) < 8 )|| (strlen($pwtext > 256)))) { //Password length
        $error .= "Tarkista salasanan pituus</br>";
        $continue = 0;
    }

    if ($continue) { //If all OK, proceed to hash password and store in DB
        $hashed = password_hash($pwtext, PASSWORD_DEFAULT);
        try {
            $stmt = $pdo->prepare('INSERT INTO users (username, pwhash) values (:username, :pwhash)');
            $stmt->execute(array(':username' => $_POST["newUser"], ':pwhash' => $hashed));
            $error = "Käyttäjätunnus luotu onnistuneesti</br>";
        } catch (PDOException $e) {
            echo "Error when inserting user to database";
        }
    }
}?>
<head>
    <title>Luo käyttäjä</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="istyle.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<nav class="topnav" id="navBar" role="full-horizontal">
    <a href="/index.php">Skinnarilan opiskelijatapahtumat</a>
    <a href="/calendar.php" title="Kalenteri">Kalenteri</a>
    <a href="/login.php" title="Kirjaudu">Kirjaudu</a>
    <a href="/newuser.php" title="Luo käyttäjä">Luo käyttäjä</a>
    <a class="navBarIcon" href="javascript:void(0);" style="font-size: 20px" onclick="openMenu()">&#8744;</a>
</nav>

<body>
    <div id="newUserDiv" style="text-align:center;">
        <p>Luo uusi käyttäjä</p>
        <p>Salasanassa on oltava vähintään 1 iso ja pieni kirjain sekä 1 numero, pituus 8-256.</br>
        Käyttäjätunnuksessa sallitaan vain alphanumeeriset merkit. (a-zA-Z0-9), pituus max 32.</p>
        <form id="createUserForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <input id="newUser" type="text" placeholder="Käyttäjätunnus" name="newUser">
            <input id="newPass" type="password" placeholder="Salasana 8-256 kirjainta" name="newPass">
            <input id="confirmPass" type="password" placeholder="Syötä salasana uudelleen" name="confirmPass">
            <button id="createUserButton" type="submit">Luo</button>
        </form>
        <div id="phperror">
            <?php echo $error; ?>
        </div>
    </div>
    <script>function openMenu() {
         let el = document.getElementById("navBar");
         if (el.className === "topnav") {
             el.className += " responsive";
         } else {
             el.className = "topnav";
         }
     }</script>
    <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
</body>
