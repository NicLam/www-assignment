<?php
//This inserts a new entry to the attending table, of the events each user is attending
if (!isset($_SESSION)) {
    session_start();
}
include('connection.php');
if (isset($_POST["eid"])) {
    try {
        $stmt = $pdo->prepare('INSERT INTO attending (uid, eid) VALUES (:uid, :eid)');
        $stmt->execute(array(':uid' => $_SESSION["uid"], ':eid' => $_POST["eid"]));
    } catch (PDOException $e) {
        echo 'Virhe syöttäessä osallistumista tietokantaan';
    }
}