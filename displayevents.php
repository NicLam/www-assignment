<?php
//Displays all upcoming events on the frontpage
include_once("connection.php");
try {
    $stmt = $pdo->prepare('SELECT eventid, ename, edesc, ewhere, ewhen, eprice FROM events WHERE ewhen >= CURDATE() ORDER BY ewhen;');
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $attending = ""; //This is used to add the "Osallistu" link to bottom of each page, only displayed if user is logged in
        if (isset($_SESSION["user"])) {
            $attending = '<tr><td colspan="2"><a class="attend" style="display:block;text-decoration:none;"href="javascript:void(0)" id="' . htmlspecialchars($row['eventid'], ENT_QUOTES, 'UTF-8') . '" >Osallistu painamalla tästä</a></td></tr>';
        }
        //Add each element into table that will be displayed
        //Note the schema itemprops for SEO, events will display on google search page
        echo '
        <table itemscope itemtype="http://schema.org/Event" class="datatable">
        <thead>
        <tr>
        <th itemprop="name" colspan="2">' . htmlspecialchars($row['ename'], ENT_QUOTES, 'UTF-8') . '</th>
        </tr>
        </thead>
        <tbody class="table-hover">
        <tr>
        <td itemprop="description" colspan="2"> ' . htmlspecialchars($row['edesc'], ENT_QUOTES, 'UTF-8') . ' </td>
        </tr><span itemprop="location" itemscope itemtype="http://schema.org/Place">

        <tr itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <td>Missä?</td>
        <td itemprop="streetAddress"> ' . htmlspecialchars($row['ewhere'], ENT_QUOTES, 'UTF-8') . '</td>
        </tr></span>
        <tr>
        <td class="text-left">Milloin?</td>
        <td class="text-left">' . htmlspecialchars(substr($row['ewhen'], 0, -3), ENT_QUOTES, 'UTF-8') . '</td>
        </tr>
        <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <td itemprop="price" class="text-left">Hinta</td>
        <td class="text-left"> ' . htmlspecialchars($row['eprice'], ENT_QUOTES, 'UTF-8') . '</td>
        </tr>' . $attending . '
        </tbody>
        </table>
        ';
    }
} catch (PDOException $e) {
    echo 'Error in database';
}